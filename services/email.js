import sendinblue from "../helpers/email-services/sendinblue"

function sendVerifyEmail(payload) {
    try {
        return sendinblue.sendVerifyEmail(payload);
    } catch (error) {
        console.log(error);
    }
}

export default { sendVerifyEmail }