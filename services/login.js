import User from "../models/User";

function findUser(email) {
    try {
        return User.findOne({ email });
    } catch (error) {
        throw error;
    }
};

export default { findUser }