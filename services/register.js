import User from "../models/User";

function saveToDB(payload) {
    try {
        const user = new User({ ...payload });
        return user.save();
    } catch (error) {
        throw error;
    }
};

function hasEmailAlready(email) {
    try {
        return User.findOne({ email });
    } catch (error) {
        throw error;
    }
};

export default { saveToDB, hasEmailAlready };