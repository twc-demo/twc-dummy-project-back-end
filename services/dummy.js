import Dummy from '../models/Dummy';

function createDummy(payload) {
  return new Dummy(payload).save();
}

function updateDummy(options, payload) {
  return Dummy.findOneAndUpdate(options, payload);
}

function deleteDummy(id) {
  return Dummy.findByIdAndDelete({ _id: id });
}

function getDummies(dummyOwner) {
  return Dummy.find({ dummyOwner }).lean();
}

export default {
  createDummy,
  updateDummy,
  deleteDummy,
  getDummies,
};
