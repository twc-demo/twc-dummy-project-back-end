import registerService from '../services/register';
import _ from 'lodash';
import bcrypt from 'bcrypt';

async function registerUser(req, res) {
    try {
        const payload = { ...req.body }
        const emailExists = await registerService.hasEmailAlready(payload.email);

        if (!_.isNull(emailExists)) {
            return res.status(400).json('email has already been used');
        }
        payload.salt = await bcrypt.genSalt(10);
        payload.password = await bcrypt.hash(payload.password, payload.salt);
        await registerService.saveToDB(payload);
        return res.status(200).json('successfully registered');
    } catch (error) {
        console.log(error);
        return res.status(500).json('server error');
    }
};

export default { registerUser }