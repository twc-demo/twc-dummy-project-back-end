import dummyService from '../services/dummy';
import { v4 as uuidv4 } from 'uuid';
import fs from 'fs';
import { promisify } from 'util';
import { Stream } from 'stream';
const pipeLine = promisify(Stream.pipeline);

export default class DummyController {
  constructor() {}
  async createDummy(req, res) {
    try {
      const dummyOwner = req.auth.userId;
      const {
        body: { name, phone, email },
        file,
      } = req;
      const imageId = uuidv4();
      const imageName = `${imageId}${file.detectedFileExtension}`;
      await pipeLine(
        file.stream,
        fs.createWriteStream(`${__dirname}/../images/${imageName}`)
      );
      await dummyService.createDummy({
        dummyOwner,
        name,
        email,
        phone,
        imageName,
      });
      return res.status(200).json('dummy created Successfully');
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  }

  async getDummies(req, res) {
    try {
      const dummyOwner = req.auth.userId;
      const dummies = await dummyService.getDummies(dummyOwner);

      dummies.map((dummy) => {
        const { imageName } = dummy;
        const file = fs.readFileSync(`${__dirname}/../images/${imageName}`);
        let base64Image = new Buffer(file, 'binary').toString('base64');
        let imgSrcString = `data:image/png;base64,${base64Image}`;
        dummy.image = imgSrcString;
        return dummy;
      });
      return res.status(200).json(dummies);
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  }

  async updateDummy(req, res) {
    try {
      const dummyOwner = req.auth.userId;
      const { name, email, phone, _id } = req.body;
      await dummyService.updateDummy(
        { dummyOwner, _id },
        { name, email, phone }
      );
      return res.status(200).json('successfully updated');
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  }

  async deleteDummy(req, res) {
    try {
      const { id } = req.params;
      const { imageName } = await dummyService.deleteDummy(id);
      fs.unlinkSync(`${__dirname}/../images/${imageName}`);
      return res.status(200).json('successfully deleted');
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  }
}
