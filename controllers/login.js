import loginService from '../services/login';
import _ from 'lodash';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken'

async function login(req, res) {
    try {
        const { email, password } = req.body;
        const user = await loginService.findUser(email);
        if (_.isNull(user)) {
            return res.status(400).json('No user found');
        }
        const inputPassword = await bcrypt.hash(password, user.salt);

        if (inputPassword === user.password) {
            const token = jwt.sign({ userId: user._id, email, firstName: user.first_name, lastName: user.last_name }, config.API_KEY, { expiresIn: '2 days' });;
            return res.status(200).json(token);
        } else {
            return res.status(401).json('Your credentials are incorrect. please check and re-enter');
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json('server error');
    }
}

export default { login }