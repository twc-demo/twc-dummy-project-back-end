const requireESM = require('esm')(module);

const app = requireESM('../app');

const port = process.env.PORT || 4040;

app.listen(port, () => {
    const { name: appName, version: appVersion } = require('../package.json');
    console.log(appName, 'is running on ', port);
});