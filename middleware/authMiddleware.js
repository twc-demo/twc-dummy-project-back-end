import jwt from 'jsonwebtoken';

function authUser(req, res, next) {
  try {
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader == 'undefined') {
      return res
        .status(403)
        .json("Invalid Token or You haven't set oauth token");
    }

    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    const authData = jwt.verify(bearerToken, config.API_KEY);
    req.auth = authData;
    next();
  } catch (error) {
    return res.status(403).json("Invalid Token or You haven't set oauth token");
  }
}

export default {
  authUser,
};
