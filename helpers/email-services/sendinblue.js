import SibApiV3Sdk from 'sendinblue-apiv3';


function init() {
    const defaultClient = SibApiV3Sdk.ApiClient.instance;
    const apiKey = defaultClient.authentications['api-key'];
    apiKey.apiKey = config.SENDINBLUE_API_KEY;
}
function sendVerifyEmail(payload) {
    try {
        init();
        const apiInstance = new SibApiV3Sdk.SMTPApi();
        const emailPayload = {
            sender: { name: 'sahan', email: 'sahan.test.18@gmail.com' },
            to: [{ name: `${payload.first_name} ${payload.last_name}`, email: payload.email }],
            subject: 'Thank you for registering with XXX',
            templateId: 1,
            params: { 'url': 'delenta.com' }
        };
        apiInstance.sendTransacEmail(emailPayload).then((data) => {
            return true;
        }, (error) => error);

    } catch (error) {
        throw error;
    }
}

export default { sendVerifyEmail }