import mongoose from 'mongoose';

function init() {
    try {

        mongoose.connect(
            config.MONGO_URL,
            { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
            error => {
                if (error) {
                    console.log('Error on connecting to MongoDB', error);
                } else {
                    console.log('Connected to MongoDB | Ready for use.');
                }
            }
        );
    } catch (error) {
        console.log(error);
    }
}

export default { init }