import { Schema, model } from 'mongoose';
import emailService from '../services/email';
import { EMAIL_STATUS } from '../constant';

const schema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    email_status: {
        type: String,
        default: EMAIL_STATUS.NOT_VERIFIED
    }
}, { collation: 'users', timestamps: true });

schema.post('save', (record) => {
    emailService.sendVerifyEmail(record);
});

export default model('User', schema);
