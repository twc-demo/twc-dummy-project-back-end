import { Schema, model } from 'mongoose';

const schema = new Schema({
    dummyOwner: {
        type: Schema.Types.ObjectId,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    imageName: {
        type: String,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
}, { collation: 'dummies', timestamps: true });

export default model('Dummy', schema);
