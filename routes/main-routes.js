import express from 'express';
const router = express.Router();
import multer from 'multer';
import registerController from '../controllers/register';
import loginController from '../controllers/login';
import dummyController from '../controllers/dummy';
import auth from '../middleware/authMiddleware';
const upload = multer();
const dummy = new dummyController();
router.get('/', (req, res) => {
  return res.status(200).json('Authentication service is running');
});

router.post('/register', registerController.registerUser);
router.post('/login', loginController.login);

router.post('/dummy/create', auth.authUser, upload.single('file'), dummy.createDummy);
router.get('/dummy/all', auth.authUser,  dummy.getDummies);
router.put('/dummy/update', auth.authUser,  dummy.updateDummy);
router.delete('/dummy/:id', auth.authUser,  dummy.deleteDummy);

export default router;
