import cors from 'cors';
import express from 'express';
import bodyParser from 'body-parser';
import dotEnv from 'dotenv';
import mongoDB from './helpers/database-connectors/mongo-connector';
import mainRoutes from './routes/main-routes';
global.config = dotEnv.config().parsed;

const app = express();
app.use(express.json());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
mongoDB.init();
app.use(cors());
app.use((req, res, next)=> {console.log(`${req.method} ${req.path}`), next()})
app.use('/twc', mainRoutes);

module.exports = app;